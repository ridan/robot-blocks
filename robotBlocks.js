var robotBlocks = function robotBlocks() {
  this.state = {};
};

robotBlocks.prototype = {
  init: function(n) {
    //initializes the index
    for(var i = 0; i < n; i++) {
      this.state[i] = [i];
    }
  },
  _indexContainingx: function(a) {
    //helper function used internally to find which index a certain element exists in
    for(index in this.state) {
      if(this.state[index].indexOf(parseInt(a)) > -1) {
        return index;
      }
    }
  },
  _reset: function(indexContaining, a) {
    //helper function used internally to reset items back to their original spot
    var index = this.state[indexContaining].indexOf(a);
    var resetIndex = index + 1;

    var totalLength = this.state[indexContaining].length;
    var remainingInArray = totalLength - resetIndex;

    for(var i = resetIndex; i <= totalLength - 1; i++) {
      var valueBeingReset = this.state[indexContaining][i];
      this.state[valueBeingReset].push(valueBeingReset);
    }
    this.state[indexContaining].splice(resetIndex);
  },
  onto: function(a, b) {
    var index = this._indexContainingx(b);
    this._reset(index, b);
  },
  over: function(a, b) {
    //does nothing this case is actually a dud
  },
  move: function(a, b) {
    var indexA = this._indexContainingx(a);
    var indexB = this._indexContainingx(b);
    this._reset(indexA, a);

    this.state[indexB].push(a);
    this.state[indexA].pop();
  },
  pile: function(a, b) {
    var indexA = this._indexContainingx(a);
    var indexB = this._indexContainingx(b);

    var indexOfMoveStack = this.state[indexA].indexOf(a);
    var stack = this.state[indexA].splice(indexOfMoveStack);
    this.state[indexB] = this.state[indexB].concat(stack);
  },
}

module.exports = {
  robotBlocks: robotBlocks
}
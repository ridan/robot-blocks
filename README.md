# RobotBlocks

This is a basic node script.

## Structure
The program is structured in 3 different files.
`instructions.txt` holds all the commands that need to passed in to the program.
`main.js` hold the program that parses the instruction file and executes the program.
`robotBlocks.js` holds the functions and logic components to the solution

## Running the program

I used the basic node version that is on my machine and pinned this app to use that node version.
If you have nvm run to get use node 6.7.0

`nvm use`

If you do not have this version then run

`nvm install 6.7.0`

Now you should have everything that you need in order to run the program. I did not want to use any external libraries or dependecies so kept everything to raw JS so there is no need for package management. To run the script run

`node main.js`

To make changes to any of the commands change them in `intructions.txt`

var robotBlocks = require("./robotBlocks.js").robotBlocks;

var firstPassDone = false;
var rb;

var lineReader = require('readline').createInterface({
  input: require('fs').createReadStream('instructions.txt')
});

lineReader.on('line', function (line) {
  if(!firstPassDone) {
    firstPassDone = true;
    rb = new robotBlocks();
    rb.init(line);

    return;
  }
  if(line === 'quit') {
    console.log(rb.state);
    return;
  }

  var instr = line.split(' ');

  var moveOrPile = instr[0];
  var ontoOrOver = instr[2];

  var A = instr[1];
  var B = instr[3];

  eval('rb.' + ontoOrOver + '('+ A +','+ B +')');
  eval('rb.' + moveOrPile + '('+ A +','+ B +')');

  // Uncomment for debugging
  // console.log(instr);
  // console.log(rb.state);

});